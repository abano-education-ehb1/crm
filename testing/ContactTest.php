<?php

use PHPUnit\Framework\TestCase;

require_once ('../src/api/AccessToken.php');
require_once ('../src/api/ApiHeaders.php');
require_once ('../src/api/Connection.php');

class ContactTest extends TestCase
{

    public function testCreateContact()
    {

        $contactXML = new \SimpleXMLElement('<user>
  <properties>
    <firstname>Jan</firstname>
    <lastname>Van Opstal</lastname>
    <email>jan.vanopstal@email.be</email>
    <address>
      <street>Lenniksesteenweg</street>
      <housenumber>4635</housenumber>
      <postalcode>1802</postalcode>
      <city>Lennik</city>
      <country>Belgie</country>
    </address>
    <phone>0456370168</phone>
    <company>
      <source-id></source-id>
      <uuid></uuid>
    </company>
  </properties>
</user>');

        //get token and headers
        $ac = new AccessToken;
        $api_token = json_decode($ac->fetchingToken());
        $apiHeader = new ApiHeaders($api_token);

        //json body voor het aanmaken van een 'contact'
        $body = '{
"data": {
  "type": "Contacts",
  "attributes": {
    "first_name": "' . $contactXML->properties->firstname . '",
    "last_name": "' . $contactXML->properties->lastname . '",
    "email1": "' . $contactXML->properties->email . '",
    "primary_address_street": "' . $contactXML->properties->address->street . ' ' . $contactXML->properties->address->housenumber . '",
    "primary_address_city": "' . $contactXML->properties->address->city . '",
    "primary_address_postalcode": "' . $contactXML->properties->address->postalcode . '",
    "primary_address_country": "' . $contactXML->properties->address->country . '",
    "phone_mobile": "' . $contactXML->properties->phone . '"
  }
}
}';

        //API call
        $contact = $this->apiCall($apiHeader, $body, 'create');

        //Assert equals
        self::assertEquals($contact->data->type, 'Contact');
    }

    public function testUpdateContact()
    {

        $conn = Connection::getApiConnection();

        $contacts = $this->getContacts($conn);

        //get token and headers
        $ac = new AccessToken;
        $api_token = json_decode($ac->fetchingToken());
        $apiHeader = new ApiHeaders($api_token);


        //json body voor het aanmaken van een 'contact'
        $body = '{
    "data": {
      "type": "Contacts",
      "id": "' . $contacts[0]['id']. '",
      "attributes": {
        "phone_office": "0495717047"
      }
    }
    }';

        //api call
        $contact = $this->apiCall($apiHeader, $body, 'update');

        //Assert equals
        self::assertEquals($contact->data->type, 'Contact');
    }

    public function testDeleteContact()
    {

        $conn = Connection::getApiConnection();

        $contacts = $this->getContacts($conn);

        //get token and headers
        $ac = new AccessToken;
        $api_token = json_decode($ac->fetchingToken());
        $apiHeader = new ApiHeaders($api_token);


//json body voor het aanmaken van een 'Account'
        $body = '{
    "data": {
      "type": "Contacts",
      "id": "' . $contacts[0]['id']. '",
      "attributes": {
      "deleted": "1"
      }
    }
}';

        $contact = $this->apiCall($apiHeader, $body, 'update');

        //Assert equals
        self::assertEquals($contact->data->type, 'Contact');
    }

    private function apiCall($apiHeader, $body, $method)
    {

        $ih = curl_init();
        $url = 'http://10.3.56.3:9000/Api/V8/module';

        //case op method en maak curl sestopt variabele
        switch ($method) {
            case 'create':
                curl_setopt($ih, CURLOPT_POST, true);
                break;
            case 'update':
                curl_setopt($ih, CURLOPT_CUSTOMREQUEST, "PATCH");
                break;
        }

        //API call
        curl_setopt($ih, CURLOPT_URL, $url); //url
        curl_setopt($ih, CURLOPT_HTTPHEADER, $apiHeader->getHeader());
        curl_setopt($ih, CURLOPT_POSTFIELDS, $apiHeader->getPostStr());
        curl_setopt($ih, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ih, CURLOPT_POSTFIELDS, $body); //json content


        return json_decode(curl_exec($ih));

    }

    private function getContacts($conn)
    {
        $contactData = array();

        $sql = "SELECT * FROM contacts";
        $result = $conn->query($sql);

        while ($row = $result->fetch_assoc()) {
            array_push($contactData, $row);
        }
        return $contactData;
    }


}