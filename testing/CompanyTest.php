<?php

use PHPUnit\Framework\TestCase;

require_once ('./src/api/AccessToken.php');
require_once ('./src/api/ApiHeaders.php');
require_once ('./src/api/Connection.php');

class CompanyTest extends TestCase{

    public function testCreateCompany()
    {

        $companyXML = new \SimpleXMLElement('<company>
    <properties>
    <name>Mcdonalds</name>
    <email>mcdo@gmail.com</email>
    <address>
      <street>Bergensesteenweg</street>
      <housenumber>4635</housenumber>
      <postalcode>1802</postalcode>
      <city>Halle</city>
      <country>Belgie</country>
    </address>
    <phone>0456915297</phone>
    <taxId>BE40047192</taxId>
  </properties>
</company>');

        //get token and headers
        $ac = new AccessToken;
        $api_token = json_decode($ac->fetchingToken());
        $apiHeader = new ApiHeaders($api_token);


        //json body voor het aanmaken van een 'company'
        $body = '{
    "data": {
      "type": "Account",
      "attributes": {
        "name": "' . $companyXML->properties->name . '",
        "email1": "' . $companyXML->properties->email . '",
        "billing_address_street": "' . $companyXML->properties->address->street . ', ' . $companyXML->properties->address->housenumber . '",
        "billing_address_city": "' . $companyXML->properties->address->city . '",
        "billing_address_country": "' . $companyXML->properties->address->country . '",
        "billing_address_postalcode": "' . $companyXML->properties->address->postalcode . '",		
        "phone_office": "' . $companyXML->properties->phone . '",
        "sic_code": "' . $companyXML->properties->taxId . '"
      }
    }
    }';

        //api call
        $company = $this->apiCall($apiHeader, $body, 'create');

        //Assert equals
        self::assertEquals($company->data->type, 'Account');
    }

    public function testUpdateCompany()
    {

        $conn = Connection::getApiConnection();

        $companies = $this->getCompanies($conn);

        //get token and headers
        $ac = new AccessToken;
        $api_token = json_decode($ac->fetchingToken());
        $apiHeader = new ApiHeaders($api_token);


        //json body voor het aanmaken van een 'company'
        $body = '{
    "data": {
      "type": "Account",
      "id": "' . $companies[0]['id']. '",
      "attributes": {
        "phone_office": "02495717047",
      }
    }
    }';

        //api call
        $company = $this->apiCall($apiHeader, $body, 'update');

        //Assert equals
        self::assertEquals($company->data->type, 'Account');
    }

    public function testDeleteCompany()
    {

       $conn = Connection::getApiConnection();

       $companies = $this->getCompanies($conn);

        //get token and headers
        $ac = new AccessToken;
        $api_token = json_decode($ac->fetchingToken());
        $apiHeader = new ApiHeaders($api_token);


//json body voor het aanmaken van een 'Account'
        $body = '{
    "data": {
      "type": "Account",
      "id": "' . $companies[0]['id']. '",
      "attributes": {
      "deleted": "1"
      }
    }
}';

        $company = $this->apiCall($apiHeader, $body, 'update');

        //Assert equals
        self::assertEquals($company->data->type, 'Account');
    }

    private function apiCall($apiHeader, $body, $method)
    {

        $ih = curl_init();
        $url = 'http://10.3.56.3:9000/Api/V8/module';

        //case op method en maak curl sestopt variabele
        switch ($method) {
            case 'create':
                curl_setopt($ih, CURLOPT_POST, true);
                break;
            case 'update':
                curl_setopt($ih, CURLOPT_CUSTOMREQUEST, "PATCH");
                break;
        }

        //API call
        curl_setopt($ih, CURLOPT_URL, $url); //url
        curl_setopt($ih, CURLOPT_HTTPHEADER, $apiHeader->getHeader());
        curl_setopt($ih, CURLOPT_POSTFIELDS, $apiHeader->getPostStr());
        curl_setopt($ih, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ih, CURLOPT_POSTFIELDS, $body); //json content


        return json_decode(curl_exec($ih));

    }

    private function getCompanies($conn)
    {
        $accountData = array();

        $sql = "SELECT * FROM accounts";
        $result = $conn->query($sql);

        while ($row = $result->fetch_assoc()) {
            array_push($accountData, $row);
        }
        return $accountData;
    }


}