<?php

use PHPUnit\Framework\TestCase;

require_once ('../src/api/AccessToken.php');
require_once ('../src/api/ApiHeaders.php');
require_once ('../src/api/Connection.php');

class SessionTest extends TestCase
{
    public function testCreateSession()
    {

        $sessionXML = new \SimpleXMLElement('<session>
  <properties>
    <name>Business Session</name>
    <beginDateTime>1653144260</beginDateTime>
    <endDateTime>1653739385</endDateTime>
    <speaker>Karl de Kearsmaker</speaker>
    <location>A1</location>
    <description>Event description.</description>
  </properties>
</session>');

        $iBeginDateTim = (int)$sessionXML->properties->beginDateTime;
        $iEndDateTime = (int)$sessionXML->properties->endDateTime;

        //get token and headers
        $ac = new AccessToken;
        $api_token = json_decode($ac->fetchingToken());
        $apiHeader = new ApiHeaders($api_token);

        //json body voor het aanmaken van een 'session'
        $body = '{
    "data": {
      "type": "FP_events",
      "attributes": {
        "name": "' . $sessionXML->properties->name . '",
        "date_start": "' . date("Y-m-d H:i:s", $iBeginDateTim) . '",
        "date_end": "' . date("Y-m-d H:i:s", $iEndDateTime) . '",
        "fp_event_locations_fp_events_1fp_event_locations_ida": "' . $sessionXML->properties->location . '",
        "description": "' . $sessionXML->properties->description . '"
      }
      }
    }';

        //API call
        $session = $this->apiCall($apiHeader, $body, 'create');

        //Assert equals
        self::assertEquals($session->data->type, 'FP_events');
    }

    public function testUpdateSession()
    {

        $conn = Connection::getApiConnection();

        $sessions = $this->getSessions($conn);

        //get token and headers
        $ac = new AccessToken;
        $api_token = json_decode($ac->fetchingToken());
        $apiHeader = new ApiHeaders($api_token);

        //json body voor het aanmaken van een 'session'
        $body = '{
    "data": {
      "type": "FP_events",
      "id": "' . $sessions[0]['id']. '",
      "attributes": {
        "description": "test descriptie",
      }
    }
    }';

        //api call
        $session = $this->apiCall($apiHeader, $body, 'update');

        //Assert equals
        self::assertEquals($session->data->type, 'FP_events');
    }

    public function testDeleteSession()
    {

        $conn = Connection::getApiConnection();

        $sessions = $this->getSessions($conn);

        //get token and headers
        $ac = new AccessToken;
        $api_token = json_decode($ac->fetchingToken());
        $apiHeader = new ApiHeaders($api_token);


//json body voor het aanmaken van een 'Session'
        $body = '{
    "data": {
      "type": "FP_events",
      "id": "' . $sessions[1]['id']. '",
      "attributes": {
      "deleted": "1"
      }
    }
}';

        $session = $this->apiCall($apiHeader, $body, 'update');

        //Assert equals
        self::assertEquals($session->data->type, 'FP_events');
    }

    private function apiCall($apiHeader, $body, $method)
    {

        $ih = curl_init();
        $url = 'http://10.3.56.3:9000/Api/V8/module';

        //case op method en maak curl sestopt variabele
        switch ($method) {
            case 'create':
                curl_setopt($ih, CURLOPT_POST, true);
                break;
            case 'update':
                curl_setopt($ih, CURLOPT_CUSTOMREQUEST, "PATCH");
                break;
        }

        //API call
        curl_setopt($ih, CURLOPT_URL, $url); //url
        curl_setopt($ih, CURLOPT_HTTPHEADER, $apiHeader->getHeader());
        curl_setopt($ih, CURLOPT_POSTFIELDS, $apiHeader->getPostStr());
        curl_setopt($ih, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ih, CURLOPT_POSTFIELDS, $body); //json content


        return json_decode(curl_exec($ih));

    }

    private function getSessions($conn)
    {
        $sessionData = array();

        $sql = "SELECT * FROM fp_events";
        $result = $conn->query($sql);

        while ($row = $result->fetch_assoc()) {
            array_push($sessionData, $row);
        }
        return $sessionData;
    }
}