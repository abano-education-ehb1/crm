<?php

require_once('../vendor/autoload.php');

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;


class UserSession
{

   public function pushUserSession($response, $channel, $file)
    {
        switch ($response->action) {
            case 'create':
                $this->createUserSession($response, $channel,$file);
                break;
            case 'delete':
                $this->deleteUserSession($response, $channel, $file);
                break;
        }
    }



    public function createUserSession($response, $channel, $file)
    {

        $conn = Connection::getApiConnection();

        $contactData = $this->getContactData($response->userId, $conn);
        $sessionData = $this->getSessionData($response->sessionId, $conn);

        $stmt = $conn->prepare("INSERT INTO fp_events_contacts_c (
    id,
    date_modified,
    deleted,
    fp_events_contactsfp_events_ida,
    fp_events_contactscontacts_idb,
    invite_status,
    accept_status,
    email_responded
)
VALUES(?,?,?,?,?,?,?,?)");

      $id = rand(1,10000);
      $date_modified = NULL;
      $deleted = 0;
      $fp_events_contactsfp_events_ida = $sessionData[0]['id'];
      $fp_events_contactscontacts_idb =$contactData[0]['id'];
      $invite_status = NULL;
      $accept_status = NULL;
      $email_responded = NULL;

      //parsedate(inpt)
       $newDate = date("Y-m-d H:i:s",$date_modified);

      $stmt->bind_param("ssissssi",$id, $newDate,$deleted,$fp_events_contactsfp_events_ida,
          $fp_events_contactscontacts_idb,$invite_status,$accept_status,$email_responded);

      $stmt->execute();

      if($stmt->error){
          echo 'user added to session';

          $this->caching($response->action, $id, $file);

      }else{
          print $stmt->error;
          $this->errorLogging($channel, 'user NOT added to session');
      }


    }

    public function deleteUserSession($response, $channel, $file)
    {
        $conn = Connection::getApiConnection();

        $contactData = $this->getContactData($response->userId, $conn);
        $sessionData = $this->getSessionData($response->sessionId, $conn);

         $conn->query("UPDATE fp_events_contacts_c set deleted = 1 WHERE fp_events_contactsfp_events_ida = '". $sessionData[0]['id'] ."' AND fp_events_contactscontacts_idb = '". $contactData[0]['id']."' ");




        /*if($stmt->error){
            echo 'user deleted from session';

            //caching
        }else{
            print $stmt->error;
            $this->errorLogging($channel, 'user NOT deleted from session');
        }*/
    }

    private function errorLogging($channel,$message){
        $date = new DateTime();

        $msg = '<error>
    <source>crm</source>
    <date>'. $date->getTimestamp() . '</date>
    <level>warn</level>
    <message>'. $message . '</message>
</error>';

        $error = new AMQPMessage($msg);

        //Error message to errors queue
        $channel->basic_publish($error, '', 'errors');
    }

    private function caching($action, $id, $file)
    {

        $current = file_get_contents($file);

        $current .= "$id \n";

        file_put_contents($file, $current);

    }

    private function getContactData($contactId, $conn)
    {
        $entityData = array();
        $sql = "SELECT * FROM contacts WHERE id= " . "'" . $contactId .  "'" . " ";

        echo $sql;

        $result = $conn->query($sql);

        while ($row = $result->fetch_assoc()) {
            array_push($entityData, $row);
        }
        return $entityData;
    }

    private function getSessionData($sessionId, $conn)
    {
        $entityData = array();

        $sql = "SELECT * FROM fp_events WHERE id= " . "'" . $sessionId .  "'" . " ";
        $result = $conn->query($sql);

        while ($row = $result->fetch_assoc()) {
            array_push($entityData, $row);
        }
        return $entityData;
    }

   }