<?php


class AccessToken
{
    function fetchingToken()
    {

        // $dotenv = Dotenv\Dotenv::createImmutable('C:\xampp\htdocs\crm');
        // $dotenv->load();

        $ch = curl_init();
        $header = array(
            'Content-type: application/vnd.api+json',
            'Accept: application/vnd.api+json'
        );

//credentials for session
        $postStr = json_encode(array(
            'grant_type' => 'client_credentials',
            'client_id' => '1d900360-70da-9611-358c-627a5ff97f58',
            'client_secret' => '123456',
        ));

// Call to access token
        $url = 'http://10.3.56.3:9000/Api/access_token';
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST'); //method
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postStr); //authenticatie
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // This option is set to TRUE so that the response does not get printed and is stored directly in the variable
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header); // content type
        $output = curl_exec($ch); //execute call

        return $output;
    }

}


?>