<?php

require_once('../vendor/autoload.php');

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

include_once('Location.php');

class Session
{

    function pushSession($response, $channel, $file)
    {

        switch ($response->action) {
            case 'create':
                $this->createSession($response, $channel, $file);
                break;
            case 'update':
                $this->updateSession($response, $channel, $file);
                break;
            case 'delete':
                $this->deleteSession($response, $channel, $file);
                break;
        }

    }

    private function createSession($response, $channel, $file)
    {
        //get token and headers
        $ac = new AccessToken;
        $api_token = json_decode($ac->fetchingToken());
        $apiHeader = new ApiHeaders($api_token);

        $location = new Location;
        $location = $location->createLocation($response->properties->location);

        $iBeginDateTim = (int)$response->properties->beginDateTime;
        $iEndDateTime = (int)$response->properties->endDateTime;

        $body = '{
    "data": {
      "type": "FP_events",
      "attributes": {
        "name": "' . $response->properties->name . '",
        "date_start": "' . date("Y-m-d H:i:s", $iBeginDateTim) . '",
        "date_end": "' . date("Y-m-d H:i:s", $iEndDateTime) . '",
        "fp_event_locations_fp_events_1fp_event_locations_ida": "' . $location . '",
        "description": "' . $response->properties->description . '"
      }
      }
    }';

        //API call
        $session = $this->apiCall($apiHeader, $body, 'create');

        if ($session->data->id) {
            echo "session CREATE in SuiteCRM \n";

            $this->caching($response->action, $session->data->id, $file);

            //locatie met event linken
            $this->pushLocationEvent($session, $location);

            $this->confirmation($channel, $session, $response);
        } else {
            $this->errorLogging($channel,'Session not created');
        }

    }

    private function updateSession($response, $channel, $file)
    {
//get token and headers
        $ac = new AccessToken;
        $api_token = json_decode($ac->fetchingToken());
        $apiHeader = new ApiHeaders($api_token);

        $location = new Location;
        $location = $location->createLocation($response->properties->location);

        $iBeginDateTim = (int)$response->properties->beginDateTime;
        $iEndDateTime = (int)$response->properties->endDateTime;


//json body voor het aanmaken van een 'contact'
        $body = '{
    "data": {
      "type": "FP_events",
       "id": "' . $response->{'source-id'} . '",
      "attributes": {
        "name": "' . $response->properties->name . '",
        "date_start": "' . date("Y-m-d H:i:s", $iBeginDateTim) . '",
        "date_end": "' . date("Y-m-d H:i:s", $iEndDateTime) . '",
        "fp_event_locations_fp_events_1fp_event_locations_ida": "' . $location . '",
        "description": "' . $response->properties->description . '"
      }
      }
    }';

        //API call
        $session = $this->apiCall($apiHeader, $body, 'update');

        if ($session->data->id) {
            echo "Session UPDATE in SuiteCRM \n";

            $this->caching($response->action, $session->data->id, $file);

            //3.Confirmation to UUID
            $this->confirmation($channel, $session, $response);

        } else {
            $this->errorLogging($channel,'Session not updated');
        }

    }

    private function deleteSession($response, $channel, $file)
    {
//get token and headers
        $ac = new AccessToken;
        $api_token = json_decode($ac->fetchingToken());
        $apiHeader = new ApiHeaders($api_token);

//json body voor het aanmaken van een 'contact'
        $body = '{
    "data": {
      "type": "FP_events",
       "id": "' . $response->{'source-id'} . '",
      "attributes": {
      "deleted": "1"
      }
        }
    }';

        //API call
        $session = $this->apiCall($apiHeader, $body, 'update');

        if ($session->data->id) {
            echo "Session DELETE in SuiteCRM \n";

            $this->caching($response->action, $session->data->id, $file);

            //3.Confirmation to UUID
            $this->confirmation($channel, $session, $response);
        } else {
            $this->errorLogging($channel,'Contact not deleted');
        }


    }

    private function apiCall($apiHeader, $body, $method)
    {

        $ih = curl_init();
        $url = 'http://10.3.56.3:9000/Api/V8/module';

        //case op method en maak curl sestopt variabele
        switch ($method) {
            case 'create':
                curl_setopt($ih, CURLOPT_POST, true);
                break;
            case 'update':
                curl_setopt($ih, CURLOPT_CUSTOMREQUEST, "PATCH");
                break;
        }

        //API call
        curl_setopt($ih, CURLOPT_URL, $url); //url
        curl_setopt($ih, CURLOPT_HTTPHEADER, $apiHeader->getHeader());
        curl_setopt($ih, CURLOPT_POSTFIELDS, $apiHeader->getPostStr());
        curl_setopt($ih, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ih, CURLOPT_POSTFIELDS, $body); //json content

        return json_decode(curl_exec($ih));

    }

    private function confirmation($channel, $session, $response)
    {
        //3.Confirmation to UUID

        $msg = '<response>
  <source>crm</source>
  <source-id>' . $session->data->id . '</source-id>
  <uuid>' . $response->uuid . '</uuid>
  <entity>session</entity>
  <action>' . $response->action . 'd</action>
</response>';

        $confirmation = new AMQPMessage($msg);

        //Confirmation to UUID queue
        $channel->basic_publish($confirmation, '', 'uuid_manager');

        echo "Session confirmation send to UUID \n";

    }

    private function errorLogging($channel,$message){
        $date = new DateTime();

        $msg = '<error>
    <source>crm</source>
    <date>'. $date->getTimestamp() . '</date>
    <level>warn</level>
    <message>'. $message . '</message>
</error>';

        $error = new AMQPMessage($msg);

        //Error message to errors queue
        $channel->basic_publish($error, '', 'errors');
    }

    private function pushLocationEvent($sessionData, $locationData)
    {

        $conn = Connection::getApiConnection();

        $stmt = $conn->prepare("INSERT INTO fp_event_locations_fp_events_1_c (
    id,
    date_modified,
    deleted,
    fp_event_locations_fp_events_1fp_event_locations_ida, 
    fp_event_locations_fp_events_1fp_events_idb
)
VALUES(?,?,?,?,?)");

        $id = rand(1, 10000);
        $date_modified = NULL;
        $deleted = 0;
        $fp_event_locations_fp_events_1fp_event_locations_ida = $sessionData->data->id;
        $fp_event_locations_fp_events_1fp_events_idb = $locationData;

        //parsedate(inpt)
        $newDate = date("Y-m-d H:i:s", $date_modified);

        $stmt->bind_param("ssiss", $id, $newDate, $deleted, $fp_event_locations_fp_events_1fp_event_locations_ida,
            $fp_event_locations_fp_events_1fp_events_idb);

        $stmt->execute();
        print $stmt->error;
    }

    private function caching($action, $id, $file)
    {

        $current = file_get_contents($file);

        $current .= "$id \n";

        file_put_contents($file, $current);

    }

    private function unixToInt($xmlDate)
    {
        return (int)$xmlDate;
    }

}