<?php

require_once('../vendor/autoload.php');

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class Company
{

    function pushCompany($response, $channel, $file)
    {

        switch ($response->action) {
            case 'create':
                $this->createCompany($response, $channel, $file);
                break;
            case 'update':
                $this->updateCompany($response, $channel, $file);
                break;
            case 'delete':
                $this->deleteCompany($response, $channel, $file);
                break;
        }

    }

    private function createCompany($response, $channel, $file)
    {
        //get token and headers
        $ac = new AccessToken;
        $api_token = json_decode($ac->fetchingToken());
        $apiHeader = new ApiHeaders($api_token);

    
        //json body voor het aanmaken van een 'company'
        $body = '{
    "data": {
      "type": "Account",
      "attributes": {
        "name": "' . $response->properties->name . '",
        "email1": "' . $response->properties->email . '",
        "billing_address_street": "' . $response->properties->address->street . ' ' . $response->properties->address->housenumber . '",
        "billing_address_city": "' . $response->properties->address->city . '",
        "billing_address_country": "' . $response->properties->address->country . '",
        "billing_address_postalcode": "' . $response->properties->address->postalcode . '",		
        "phone_office": "' . $response->properties->phone . '",
        "description": "' . $response->properties->taxId . '"
      }
    }
    }';

        //api call
        $company = $this->apiCall($apiHeader, $body, 'create');

        if ($company->data->id) {
            echo "Company CREATE in SuiteCRM \n";

            $this->caching($response->action, $company->data->id, $file);

            //3.Confirmation to UUID
            $this->confirmation($channel, $company, $response);

        } else {
            $this->errorLogging($channel,'Company not created');
        }
    }

    private function updateCompany($response, $channel, $file)
    {

        //get token and headers
        $ac = new AccessToken;
        $api_token = json_decode($ac->fetchingToken());
        $apiHeader = new ApiHeaders($api_token);


//json body voor het aanmaken van een 'contact'
        $body = '{
    "data": {
      "type": "Account",
      "id": "' . $response->{'source-id'} . '",
      "attributes": {
        "name": "' . $response->properties->name . '",
        "email1": "' . $response->properties->email . '",
        "billing_address_street": "' . $response->properties->address->street . ' ' . $response->properties->address->housenumber . '",
        "billing_address_city": "' . $response->properties->address->city . '",
        "billing_address_country": "' . $response->properties->address->country . '",
        "billing_address_postalcode": "' . $response->properties->address->postalcode . '",		
        "phone_office": "' . $response->properties->phone . '",
        "description": "' . $response->properties->taxId . '"
      }
    }
    }';


        $company = $this->apiCall($apiHeader, $body, 'update');

        if ($company->data->id) {
            echo "Company UPDATE in SuiteCRM \n";
            //AND cache
            $this->caching($response->action, $company->data->id, $file);

            //3.Confirmation to UUID
            $this->confirmation($channel, $company, $response);
        }
        else{
            $this->errorLogging($channel,'Company not updated');
        }

    }

    private function deleteCompany($response, $channel, $file)
    {

//get token and headers
        $ac = new AccessToken;
        $api_token = json_decode($ac->fetchingToken());
        $apiHeader = new ApiHeaders($api_token);


//json body voor het aanmaken van een 'Account'
        $body = '{
    "data": {
      "type": "Account",
      "id": "' . $response->{'source-id'} . '",
      "attributes": {
      "deleted": "1"
      }
    }
}';

        $company = $this->apiCall($apiHeader, $body, 'update');

        if ($company->data->id) {
            echo "Company DELETE in SuiteCRM \n";
            //AND cache
            $this->caching($response->action, $company->data->id, $file);

            //3.Confirmation to UUID
            $this->confirmation($channel, $company, $response);
        }
        else{
            $this->errorLogging($channel,'Company not deleted');
        }
    }

    private function apiCall($apiHeader, $body, $method)
    {

        $ih = curl_init();
        $url = 'http://10.3.56.3:9000/Api/V8/module';

        //case op method en maak curl sestopt variabele
        switch ($method) {
            case 'create':
                curl_setopt($ih, CURLOPT_POST, true);
                break;
            case 'update':
                curl_setopt($ih, CURLOPT_CUSTOMREQUEST, "PATCH");
                break;
        }

        //API call
        curl_setopt($ih, CURLOPT_URL, $url); //url
        curl_setopt($ih, CURLOPT_HTTPHEADER, $apiHeader->getHeader());
        curl_setopt($ih, CURLOPT_POSTFIELDS, $apiHeader->getPostStr());
        curl_setopt($ih, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ih, CURLOPT_POSTFIELDS, $body); //json content


        return json_decode(curl_exec($ih));

    }

    private function confirmation($channel, $company, $response)
    {

        $msg = '<response>
  <source>crm</source>
  <source-id>' . $company->data->id . '</source-id>
  <uuid>' . $response->uuid . '</uuid>
  <entity>company</entity>
  <action>' . $response->action . 'd</action>
</response>';

        $confirmation = new AMQPMessage($msg);

        //Confirmation to UUID queue
        $channel->basic_publish($confirmation, '', 'uuid_manager');

        echo "Company confirmation send to UUID \n";

    }

    private function errorLogging($channel,$message){
        $date = new DateTime();

        $msg = '<error>
    <source>crm</source>
    <date>'. $date->getTimestamp() . '</date>
    <level>warn</level>
    <message>'. $message . '</message>
</error>';

        $error = new AMQPMessage($msg);

        //Error message to errors queue
        $channel->basic_publish($error, '', 'errors');
    }

    private function caching($action, $id, $file)
    {

        $current = file_get_contents($file);

        $current .= "$id \n";

        file_put_contents($file, $current);

    }
}