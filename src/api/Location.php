<?php

require_once('../vendor/autoload.php');
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class Location{

    function createLocation($location)
    {

        $conn = Connection::getApiConnection();

        $locationData = $this->getLocations($conn);

        //Check if Location exists
        $locationInIt = false;
        foreach($locationData as $locationName){
           /* global $locationInIt;*/
            if($location == $locationName['name']){
                $locationInIt = true;
              return $locationName['id'];
            }
        }

        if(!$locationInIt){
            echo "new location \n";
            $ac = new AccessToken;

            //omzetten van json naar object
            $api_token = json_decode($ac->fetchingToken());

            //header aanpassen met api token
            $header = array(
                'Content-type: application/vnd.api+json',
                "Authorization: Bearer $api_token->access_token"
            );

            //credentials for location
            $postStr = json_encode(array(
                'grant_type' => 'client_credentials',
                'client_id' => 'abf109dc-3439-e64f-1ed2-6262b52a6775',
                'client_secret' => '123456',
            ));

            //json body voor het aanmaken van een 'location'
            $body = '{
    "data": {
      "type": "FP_Event_Locations",
      "attributes": {
        "name": "' . $location . '"
      }
      }
    }';
            //API POST call
            $ih = curl_init();
            $url = 'http://10.3.56.3:9000/Api/V8/module';
            curl_setopt($ih, CURLOPT_URL, $url); //url
            curl_setopt($ih, CURLOPT_POST, true);
            curl_setopt($ih, CURLOPT_HTTPHEADER, $header);
            curl_setopt($ih, CURLOPT_POSTFIELDS, $postStr);
            curl_setopt($ih, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ih, CURLOPT_POSTFIELDS, $body); //json content

            return json_decode(curl_exec($ih))->data->id;

        }

    }

    function getLocations($conn){
        $locationData = array();

        $sql = "SELECT * FROM fp_event_locations ";
        $result = $conn->query($sql);

        while ($row = $result->fetch_assoc()) {
            array_push($locationData, $row);
        }
        return $locationData;
    }


}