<?php

require_once('../vendor/autoload.php');

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class Contact
{

    function pushContact($response, $channel, $file)
    {
        switch ($response->action) {
            case 'create':
                $this->createContact($response, $channel, $file);
                break;
            case 'update':
                $this->updateContact($response, $channel, $file);
                break;
            case 'delete':
                $this->deleteContact($response, $channel, $file);
                break;
        }
    }

    private function createContact($response, $channel, $file)
    {
        //get token and headers
        $ac = new AccessToken;
        $api_token = json_decode($ac->fetchingToken());
        $apiHeader = new ApiHeaders($api_token);

        $description = '';
        $company = '';

        if ($this->checkCompany($response)) {
            $description = "Employer " . $company . " ";
        }


//json body voor het aanmaken van een 'contact'
        $body = '{
"data": {
  "type": "Contacts",
  "attributes": {
    "first_name": "' . $response->properties->firstname . '",
    "last_name": "' . $response->properties->lastname . '",
    "email1": "' . $response->properties->email . '",
    "primary_address_street": "' . $response->properties->address->street . ' ' . $response->properties->address->housenumber . '",
    "primary_address_city": "' . $response->properties->address->city . '",
    "primary_address_postalcode": "' . $response->properties->address->postalcode . '",
    "primary_address_country": "' . $response->properties->address->country . '",
    "phone_mobile": "' . $response->properties->phone . '",
    "description": "' . $description . '" 
  }
}
}';

        //API call
        $contact = $this->apiCall($apiHeader, $body, 'create');

        if ($contact->data->id) {
            echo "contact CREATE in SuiteCRM \n";

            $this->caching($response->action, $contact->data->id, $file);

            //3.Confirmation to UUID
            $this->confirmation($channel, $contact, $response);

        } else {
            $this->errorLogging($channel,'Contact not created');
        }
    }

    private function updateContact($response, $channel, $file)
    {

        //get token and headers
        $ac = new AccessToken;
        $api_token = json_decode($ac->fetchingToken());
        $apiHeader = new ApiHeaders($api_token);


        $description = '';
        $company = '';

        if ($this->checkCompany($response)) {
            $description = "Employer " . $company . " ";
        }


//json body voor het aanmaken van een 'contact'
        $body = '{
"data": {
  "type": "Contacts",
  "id": "' . $response->{'source-id'} . '",
  "attributes": {
    "first_name": "' . $response->properties->firstname . '",
    "last_name": "' . $response->properties->lastname . '",
    "email1": "' . $response->properties->email . '",
    "primary_address_street": "' . $response->properties->address->street . ' ' . $response->properties->address->housenumber . '",
    "primary_address_city": "' . $response->properties->address->city . '",
    "primary_address_postalcode": "' . $response->properties->address->postalcode . '",
    "primary_address_country": "' . $response->properties->address->country . '",
    "phone_mobile": "' . $response->properties->phone . '",
    "description": "' . $description . '"
  }
}
}';

//API call
        $contact = $this->apiCall($apiHeader, $body, 'update');

        if ($contact->data->id) {
            echo "contact UPDATE in SuiteCRM \n";

            $this->caching($response->action, $contact->data->id, $file);

            //3.Confirmation to UUID
            $this->confirmation($channel, $contact, $response);
        } else {
            $this->errorLogging($channel,'Contact not updated');
        }


    }

    private function deleteContact($response, $channel, $file)
    {

        //get token and headers
        $ac = new AccessToken;
        $api_token = json_decode($ac->fetchingToken());
        $apiHeader = new ApiHeaders($api_token);

//json body voor het aanmaken van een 'Contact'
        $body = '{
"data": {
  "type": "Contacts",
  "id": "' . $response->{'source-id'} . '",
  "attributes": {
      "deleted": "1"
      }
    }
}';


        //API call
        $contact = $this->apiCall($apiHeader, $body, 'update');

        if ($contact->data->id) {
            echo "contact DELETE in SuiteCRM \n";

            $this->caching($response->action, $contact->data->id, $file);

            //3.Confirmation to UUID
            $this->confirmation($channel, $contact, $response);

        } else {
            $this->errorLogging($channel,'Contact not deleted');
        }
    }

    private function apiCall($apiHeader, $body, $method)
    {

        $ih = curl_init();
        $url = 'http://10.3.56.3:9000/Api/V8/module';

        //case op method en maak curl sestopt variabele
        switch ($method) {
            case 'create':
                curl_setopt($ih, CURLOPT_POST, true);
                break;
            case 'update':
                curl_setopt($ih, CURLOPT_CUSTOMREQUEST, "PATCH");
                break;
        }

        //API call
        curl_setopt($ih, CURLOPT_URL, $url); //url
        curl_setopt($ih, CURLOPT_HTTPHEADER, $apiHeader->getHeader());
        curl_setopt($ih, CURLOPT_POSTFIELDS, $apiHeader->getPostStr());
        curl_setopt($ih, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ih, CURLOPT_POSTFIELDS, $body); //json content

        return json_decode(curl_exec($ih));

    }

    private function confirmation($channel, $contact, $response)
    {

        $msg = '<response>
  <source>crm</source>
  <source-id>' . $contact->data->id . '</source-id>
  <uuid>' . $response->uuid . '</uuid>
  <entity>user</entity>
  <action>' . $response->action . 'd</action>
</response>';

        $confirmation = new AMQPMessage($msg);

//Confirmation to UUID queue
        $channel->basic_publish($confirmation, '', 'uuid_manager');

        echo "Contact confirmation send to UUID \n";
    }

    private function errorLogging($channel,$message){
        $date = new DateTime();

        $msg = '<error>
    <source>crm</source>
    <date>'. $date->getTimestamp() . '</date>
    <level>warn</level>
    <message>'. $message . '</message>
</error>';

        $error = new AMQPMessage($msg);

        //Error message to errors queue
        $channel->basic_publish($error, '', 'errors');
    }

    private function getCompanies($conn)
    {
        $accountData = array();

        $sql = "SELECT * FROM accounts";
        $result = $conn->query($sql);

        while ($row = $result->fetch_assoc()) {
            array_push($accountData, $row);
        }
        return $accountData;
    }

    //Check if company exist
    private function checkCompany($response)
    {
        $accCheck = false;
        global $company; //global?

        if (!$response->properties->company->{'source-id'} == null) {
            //zoek op company id de naam op , vergelijk
            $companys = $this->getCompanies(Connection::getApiConnection());
            foreach ($companys as $c) {
                if ($c['id'] == $response->properties->company->{'source-id'}) {
                    $company = $c['name'];
                    $accCheck = true;
                }
            }
        }

        if (!$accCheck) {
            echo "Not a correspondending company in Database \n";
            //throw error to monitoring
        }
        return $accCheck;
    }

    private function caching($action, $id, $file)
    {

        $current = file_get_contents($file);

        $current .= "$id \n";

        file_put_contents($file, $current);

    }
}

