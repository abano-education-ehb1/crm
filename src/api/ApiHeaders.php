<?php

// $dotenv = Dotenv\Dotenv::createImmutable('C:\xampp\htdocs\crm');
// $dotenv->load();

class ApiHeaders{

public $header;
public $postStr;

 function __construct($api_token)
{
      //header aanpassen met api token
    $this->header = array(
        'Content-type: application/vnd.api+json',
        "Authorization: Bearer $api_token->access_token"
    );

    //credentials for session
   $this->postStr = json_encode(array(
        'grant_type' => 'client_credentials',
        'client_id' => 'abf109dc-3439-e64f-1ed2-6262b52a6775',
        'client_secret' => '123456'
    ));

}


    public function getHeader(): array
    {
        return $this->header;
    }


    public function getPostStr()
    {
        return $this->postStr;
    }
}