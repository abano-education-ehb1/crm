<?php
require_once('../vendor/autoload.php');

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class PEvents
{

    function PublishEvent($eventAction, $eventId)
    {

//connection to RabbitMQ
        $connection = new AMQPStreamConnection('10.3.56.3', 5672, 'integration_project22', '3jPqsaCUBXgHHLzM');
        $channel = $connection->channel();

//declare queue to send message to
        $channel->queue_declare('uuid_manager', 'fanout', false, false, false);

        $conn = Connection::getApiConnection();

        $entityData = $this->getEventData($eventId, $conn);
        $locationEventData = $this->getLocationEvent($eventId, $conn);
        $location = $this->getLocationData($locationEventData,$conn);

        //speaker location en description ontleden
        $description = explode(',', $entityData[0]['description']);

        if($entityData[0]['deleted']){
            $eventAction = 'delete';
        }

        foreach ($entityData as $entity) {

            $msg = '<session>
  <source>crm</source>  
  <action>' . $eventAction . '</action>
  <source-id>' . $eventId . '</source-id>
  <uuid></uuid>
  <properties>
    <name>' . $entity['name'] . '</name>
    <beginDateTime>' . strtotime($entity['date_start']) . '</beginDateTime>
    <endDateTime>' . strtotime($entity['date_end']) . '</endDateTime>
    <speaker></speaker>
    <location>' . $location[0]['name'] . '</location>
    <description>' . $entity['description'] . '</description>
  </properties>
</session>';

            echo " .$msg. \n";

            $updatedEvent = new AMQPMessage($msg);

            //publish to queue
            $channel->basic_publish($updatedEvent, '', 'uuid_manager');

            echo "Session updated \n";

        }
        $channel->close();
        $connection->close();
    }

    private function getEventData($eventId, $conn)
    {
        $entityData = array();

        $sql = "SELECT * FROM fp_events WHERE id= " . "'" . $eventId . "'" . " ";
        $result = $conn->query($sql);

        while ($row = $result->fetch_assoc()) {
            array_push($entityData, $row);
        }
        return $entityData;
    }

    private function getLocationEvent($eventId, $conn)
    {
        $entityData = array();

        $sql = "SELECT * FROM fp_event_locations_fp_events_1_c WHERE fp_event_locations_fp_events_1fp_events_idb= " . "'" . $eventId . "'" . " AND
         deleted = 0";
        $result = $conn->query($sql);

        while ($row = $result->fetch_assoc()) {
            array_push($entityData, $row);
        }
        return $entityData;
    }

    private function getLocationData($locationEventData,$conn)
    {
        $entityData = array();

        $sql = "SELECT * FROM fp_event_locations 
         WHERE id= " . "'" . $locationEventData[0]['fp_event_locations_fp_events_1fp_event_locations_ida'] . "'" . " ";
        $result = $conn->query($sql);

        while ($row = $result->fetch_assoc()) {
            array_push($entityData, $row);
        }
        return $entityData;
    }
}


?>