<?php
require_once('../vendor/autoload.php');

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class PCompany
{

    function PublishAccount($accountAction, $accountId)
    {

//connection to RabbitMQ
        $connection = new AMQPStreamConnection('10.3.56.3', 5672, 'integration_project22', '3jPqsaCUBXgHHLzM');
        $channel = $connection->channel();

//declare queue to send message to
        $channel->queue_declare('uuid_manager', 'fanout', false, false, false);

        $conn = Connection::getApiConnection();

        $entityData = $this->getAccountData($accountId, $conn);
        $entityEmail = $this->getAccountEmail($this->getEmailId($accountId, $conn),$conn);

        $addressLine1 = trim($entityData[0]['billing_address_street']);
        $lastSpaceIndex = strrpos($addressLine1, " ");
        $street = substr($addressLine1, 0, $lastSpaceIndex);
        $housenumber = substr($addressLine1, $lastSpaceIndex);

        if($entityData[0]['deleted']){
            $accountAction = 'delete';
        }

        //default waarden
        if(empty($entityData[0]['sic_code'])){
            $entityData[0]['sic_code'] = '0255710113';
        }

        foreach ($entityData as $entity) {

            $msg = '<company>
  <source>crm</source>
  <source-id>' . $entity['id'] . '</source-id>
  <uuid></uuid>
  <action>' . $accountAction . '</action>
  <properties>
    <name>' . $entity['name'] . '</name>
    <email>' . $entityEmail[0]['email_address'] . '</email>
    <address>
      <street>' . $street . '</street>
      <housenumber>' . $housenumber . '</housenumber>
      <postalcode>' . $entity['billing_address_postalcode'] . '</postalcode>
      <city>' . $entity['billing_address_city'] . '</city>
      <country>' . $entity['billing_address_country'] . '</country>
    </address>
    <phone>' . $entity['phone_office'] . '</phone>
    <taxId>BE' . $entity['description'] . '</taxId>
  </properties>
</company>';

            echo " .$msg. \n";

            $updatedAccount = new AMQPMessage($msg); // dit word xml message (JSON omzetten naar xml)

            //Geupdate entity sturen naar rabbitMQ

            //publish to queue
            $channel->basic_publish($updatedAccount, '', 'uuid_manager');

            echo "Company Updated \n";

        }
        $channel->close();
        $connection->close();
    }

    //get account data
    private function getAccountData($accountId, $conn)
    {
        $entityData = array();

        $sql = "SELECT * FROM accounts WHERE id= " . "'" . $accountId . "'" . " ";
        $result = $conn->query($sql);

        while ($row = $result->fetch_assoc()) {
            array_push($entityData, $row);
        }
        return $entityData;
    }

    //get contact email id in linking table
    private function getEmailId($accountId, $conn)
    {
        $entityEmailId = array();

        $sql = "SELECT email_address_id FROM email_addr_bean_rel WHERE bean_id= " . "'" . $accountId . "'" . " ";
        $result = $conn->query($sql);

        while ($row = $result->fetch_assoc()) {
            array_push($entityEmailId, $row);
        }
        return $entityEmailId;
    }

    //get contact email
    private function getAccountEmail($entityEmailId,$conn)
    {
        $entityEmail = array();

        $sql = "SELECT * FROM email_addresses WHERE id= " . "'" . $entityEmailId[0]['email_address_id'] . "'" . " ";
        $result = $conn->query($sql);

        while ($row = $result->fetch_assoc()) {
            array_push($entityEmail, $row);
        }
        return $entityEmail;
    }


}