<?php
require_once('../vendor/autoload.php');

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class PContact
{

    function PublishContact($contactAction, $contactId)
    {

//connection to RabbitMQ
        $connection = new AMQPStreamConnection('10.3.56.3', 5672, 'integration_project22', '3jPqsaCUBXgHHLzM');
        $channel = $connection->channel();

//declare queue to send message to
        $channel->queue_declare('uuid_manager', 'fanout', false, false, false);

        $conn = Connection::getApiConnection();

        $entityData = $this->getContactData($contactId, $conn);
        $entityEmail = $this->getContactEmail($this->getEmailId($contactId, $conn), $conn);

        //Description vergelijken met accounts uit tabel
        $accounts = $this->getCompanyData($conn);
        $account = array();
        $check= false;

        //explode description
        $accountName = explode(' ', $entityData[0]['description']);

        //Check Company name to get source id
        if(!$accountName[0] == null) {
            foreach ($accounts as $a) {
                if ($accountName[1] == $a['name']) {
                    $check = true;
                    $account = $a;
                }
            }
        }else{
            $account['id'] = '';
        }

        if(!$check){
            //no corresponding company..
        }

        //opsplitsen in straat en nummer
        //$address = explode(',', $entityData[0]['primary_address_street']);
        $addressLine1 = trim($entityData[0]['primary_address_street']);
        $lastSpaceIndex = strrpos($addressLine1, " ");
        $street = substr($addressLine1, 0, $lastSpaceIndex);
        $housenumber = substr($addressLine1, $lastSpaceIndex);

        if($entityData[0]['deleted']){
            $contactAction = 'delete';
        }

        //elk entity doorsturen
        foreach ($entityData as $entity) {
            $msg = '<user>
    <source>crm</source>
    <source-id>' . $entity['id'] . '</source-id>
    <uuid></uuid>
    <action>' . $contactAction . '</action>
    <properties>
      <firstname>' . $entity['first_name'] . '</firstname>
      <lastname>' . $entity['last_name'] . '</lastname>
      <email>' . $entityEmail[0]['email_address'] . '</email>
      <address>
        <street>' . $street . '</street>
        <housenumber>' . $housenumber . '</housenumber>
        <postalcode>' . $entity['primary_address_postalcode'] . '</postalcode>
        <city>' . $entity['primary_address_city'] . '</city>
        <country>' . $entity['primary_address_country'] . '</country>
      </address>
      <phone>' . $entity['phone_mobile'] . '</phone>
      <company>
        <source-id>'.$account['id'].'</source-id> 
        <uuid></uuid>
      </company>
    </properties>
    </user>';

            echo " .$msg. \n";

            $updatedContact = new AMQPMessage($msg); // dit word xml message (JSON omzetten naar xml)

            //Geupdate entity sturen naar rabbitMQ

            //publish to queue
            $channel->basic_publish($updatedContact, '', 'uuid_manager');
            echo "Contact Updated \n";


        }

        $channel->close();
        $connection->close();
    }

    //get contact data
    private function getContactData($contactId, $conn)
    {
        $entityData = array();

        $sql = "SELECT * FROM contacts WHERE id= " . "'" . $contactId . "'" . " ";
        $result = $conn->query($sql);

        while ($row = $result->fetch_assoc()) {
            array_push($entityData, $row);
        }
        return $entityData;
    }

    //get contact email id in linking table
    private function getEmailId($contactId, $conn)
    {
        $entityEmailId = array();

        $sql = "SELECT email_address_id FROM email_addr_bean_rel WHERE bean_id= " . "'" . $contactId . "'" . " ";
        $result = $conn->query($sql);

        while ($row = $result->fetch_assoc()) {
            array_push($entityEmailId, $row);
        }

        return $entityEmailId;

    }

    //get contact email
    private function getContactEmail($entityEmailId, $conn)
    {
        $entityEmail = array();

        $sql = "SELECT * FROM email_addresses WHERE id= " . "'" . $entityEmailId[0]['email_address_id'] . "'" . " ";
        $result = $conn->query($sql);

        while ($row = $result->fetch_assoc()) {
            array_push($entityEmail, $row);
        }

        return $entityEmail;
    }

    private function getCompanyData($conn)
    {
        $accountData = array();

        $sql = "SELECT * FROM accounts";
        $result = $conn->query($sql);

        while ($row = $result->fetch_assoc()) {
            array_push($accountData, $row);
        }
        return $accountData;
    }


}