<?php

include_once('./api/Includes.php');

// Create connection
$conn = Connection::getApiConnection();

// Check connection
if ($conn->connect_error) {
    die("ApiConnection.php failed: " . $conn->connect_error);
}

//variables
$checkUpdateData = array();
$file = './cache/cache.txt';
$cacheCheck = false;

$loop_expiry_time = time() + 60;

while (time() < $loop_expiry_time) {

//Call to CheckUpdates table #1
    $sql = "SELECT * FROM CheckUpdates";
    $result = $conn->query($sql);

//Get action, table and id from CheckUpdate
    if ($result->num_rows > 0) {

        // loop over checkupdate table and push to array
        while ($row = $result->fetch_assoc()) {
            array_push($checkUpdateData, $row);
        }

//Check if updateRow == cacheRow
        $cacheCheck = checkCache($checkUpdateData, $file);

         if ($cacheCheck == false) {
             foreach ($checkUpdateData as $checkUpdateRow) {

                 // switch case naar publisher classes
                 switch ($checkUpdateRow['entity']) {
                     case "contacts":
                         $contact = new PContact();
                         $contact->PublishContact($checkUpdateRow['action'], $checkUpdateRow['rel_id']);
                     case "accounts":
                         $account = new PCompany();
                         $account->PublishAccount($checkUpdateRow['action'], $checkUpdateRow['rel_id']);
                     case "events":
                         $event = new PEvents();
                         $event->PublishEvent($checkUpdateRow['action'], $checkUpdateRow['rel_id']);
                 }
             }
         }


        //delete all rows of CheckUpdates table (beveiliging ergens?)
         $sql = "Delete FROM CheckUpdates";
         $result = $conn->query($sql);

        //And clear cache
        unlink($file);

        sleep(10);

    } else {
        //do nothing
    }
    sleep(10);
}

$conn->close();

function checkCache($checkUpdateData, $file)
{
    $uuids = explode("\n", file_get_contents($file));
    foreach ($checkUpdateData as $checkUpdateRow) {
        for ($i = 0; $i <= count($uuids); $i++) {
            if (trim($checkUpdateRow['rel_id']) == trim(trim($uuids[$i], "\n"))) {
                return true;
            }
        }
    }
}

?>