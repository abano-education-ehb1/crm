<?php
require_once __DIR__ . '../../vendor/autoload.php';

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

include_once('./api/Includes.php');

//connection to RabbitMQ
$connection = new AMQPStreamConnection('10.3.56.3',5672, 'integration_project22', '3jPqsaCUBXgHHLzM');
$channel = $connection->channel();

//Queue declarations
$channel->queue_declare('uuid_manager', 'fanout', false, false, false);
$channel->queue_declare('errors', false, false, false, false);
$channel->queue_declare('crm', false, false, false, false);

echo " [*] Waiting for logs. To exit press CTRL+C\n";

//1. Recieve message 
//hier zal de xml zitten in msg->body 
$callback = function ($msg) {
    echo " . $msg->body . \n";

    $response = $msg->body;

    //caching file aanmaken
    fopen('./cache/cache.txt', 'a');
    $file = './cache/cache.txt';

//2. XSD VALIDATION
    $xml = new DOMDocument();
    $xml->loadXML($response, LIBXML_NOBLANKS);
    $response = new SimpleXMLElement($response);

    //variables
    global $channel;

    if ($xml->schemaValidate('./xsd/user.xsd')) {
        $contact = new Contact;
        $contact->pushContact($response, $channel, $file);
    } elseif ($xml->schemaValidate('./xsd/company.xsd')) {
        $company = new Company;
        $company->pushCompany($response, $channel, $file);
    } else if ($xml->schemaValidate('./xsd/session.xsd')) {
        $session = new Session;
        $session->pushSession($response, $channel, $file);
    } else if ($xml->schemaValidate('./xsd/user-session.xsd')) {
        $userSession = new UserSession;
        $userSession->pushUserSession($response, $channel, $file);
    } else {
        echo 'NO VALID XSD';
    }

};

//zeg aan de fanout dat message is consumed??
$channel->basic_consume('crm', '', false, true, false, false, $callback);


while ($channel->is_open()) {
    $channel->wait();
}

$channel->close();
$connection->close();
?>