<?php

require_once __DIR__ . '../../vendor/autoload.php';
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

//connection to RabbitMQ
$connection = new AMQPStreamConnection('10.3.56.3', 5672, 'integration_project22', '3jPqsaCUBXgHHLzM');
$channel = $connection->channel();

//declare queue to send message to 
$channel->queue_declare('monitoring', false, false, false, false);

$loop_expiry_time = time() + 60;

while (time() < $loop_expiry_time) {
    $date = new DateTime();

    $msg = '<heartbeat>
    <source>crm</source>
    <date>'. $date->getTimestamp() . '</date>
  </heartbeat>';

    $beat = new AMQPMessage($msg);

    echo 'beat';

    //publish to queue
    $channel->basic_publish($beat,'', 'monitoring'); //every 3 sec

    sleep(3);
}














?>